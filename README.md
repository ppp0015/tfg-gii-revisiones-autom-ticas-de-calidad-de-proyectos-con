# Revisiones automáticas de calidad de proyectos software con SonarQube #

![dashboard_remoto.png](https://bitbucket.org/repo/j4GqGX/images/2546301902-dashboard_remoto.png)
## Descripción

Este proyecto se ha creado con el objetivo de mejorar el [entorno de evaluación](https://dl.dropboxusercontent.com/u/18996787/TFGII/web/MetricSist.html) de Trabajos Fin de Grado, desarrollada desde el area de Lenguajes y Sistemas Informáticos de la Universidad de Burgos.

La mejora consiste en crear una plataforma Web, basada en la herramienta SonarQube, que permite medir la calidad de proyectos y mostrar los resultados en un *dashboard*. En el siguiente [enlace](http://tfgqa-ubugii.rhcloud.com/) se puede ver el estado de evaluación de la instancia actual.

Con la realización del proyecto se pretende introducir cambios, que ayuden en la evaluación de la calidad de los TFG del Grado de Ingeniería Informática de la Universidad de Burgos.

**Este proyecto ha sido realizado como Trabajo Fin de Grado del Grado en Ingeniería Informática en la Universidad de Burgos.**

## Autor
* **Plamen Petyov Petkov**

## Tutor
* ** Carlos López Nozal**