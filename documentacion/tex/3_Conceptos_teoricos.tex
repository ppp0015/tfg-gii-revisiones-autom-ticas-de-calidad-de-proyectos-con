\capitulo{3}{Conceptos teóricos}

% En aquellos proyectos que necesiten para su comprensión y desarrollo de unos conceptos teóricos de una determinada materia o % de un determinado dominio de conocimiento, debe existir un apartado que sintetice dichos conceptos.

\section{Calidad y Software}
Hay muchas y diferentes formas de definir la calidad de un producto, por lo que a menudo resulta complicado dar una definición concreta. 
Por ejemplo, para un usuario, la satisfacción al utilizar un producto es sinónimo calidad. Si el producto satisface las necesidades del usuaro, se dice que presenta calidad. Desde el punto de vista de un desarrollador, la calidad se define en términos de especificaciones del producto, es decir, si el producto final cumple las especificaciones inicialmente establecidas, presenta calidad.

A pesar de que se trata de un concepto subjetivo, existe una definición formalizada por el estándar ISO (\textit{International Organization for Standardization})\footnote{Página oficial de ISO: \url{http://www.iso.org/iso/home.html}}, que define la calidad como \textit{''el conjunto de propiedades y características de un producto o servicio que le confinen su aptitud para satisfacer unas necesidades expresadas o implícitas''} \cite{dolado_medicion_2000}. Se asume además, que la calidad está estrechamente relacionada con las distintas etapas en el desarrollo de un producto. Por eso es importante la adopción de políticas de gestión y aseguramiento de la calidad durante el proceso de desarrollo.

La preocupación por la calidad de software surge en los años 60 del siglo pasado con el desarrollo de los primeros sistemas software de gran tamaño. El software que se creaba era lento, de baja fiabilidad, difícil de mantener y reutilizar. Esto originó la adopción de técnicas de gestión de la calidad de software, derivadas de metodologías usadas en la manufacturación industrial. En concreto, en la gestión de la calidad de software, se adoptaron normas del estándar ISO 9000\footnote{ISO 9000 en Wikipedia: \url{https://en.wikipedia.org/wiki/ISO_9000}}, que posteriormente dieron lugar a otras normas específicas para evaluar la calidad de productos software, que se basaban en el proceso de desarrollo como medio de aseguramiento. Una de estas normas es el estándar ISO 9126\footnote{ISO 9126 en Wikipedia: \url{https://en.wikipedia.org/wiki/ISO/IEC_9126}}(sección \ref{iso9126}), desglosado en cuatro partes, cada una de las cuales se ocupa de aspectos diferentes de la calidad.

En un sentido general, la calidad de software puede definirse como \textit{''la aplcación de un proceso efectivo de desarrollo de software a partir del cual se obtenga un producto útil, que proporcione valores medibles para quienes lo producen y para quienes lo usen''}\footnote{Definición adaptada de Pressman \cite{pressman}, capítulo 14, pág. 400}.

 
\section{Gestión y Aseguramiento de la Calidad}
La gestión de la calidad de software se ocupa principalmente de dos cosas:
\begin{itemize}
	\item{A nivel de organización, la gestión de la calidad de software tiene como objetivo establecer un marco que conduzca al desarrollo de software de alta calidad. Esto implica la definición de los procesos de desarrollo a utilizar así como los estándares a aplicar a los distintos productos obtenidos (código fuente, documentación, requisitos, etc.).}
	\item{A nivel de proyecto, la gestión de la calidad involucra la aplicación de procesos de calidad específicos, comprobar que se han seguido dichos procesos y asegurar que los productos obtenidos se ajustan a los estándares aplicables a ese proyecto. La gestión de la calidad también se ocupa de definir un plan de calidad específico a seguir durante el desarrollo del proyecto.}
\end{itemize}

\imagen{gestion_calidad_organizacion.png}{Gestión de la calidad a nivel de organización.}

El aseguramiento de la calidad (del inglés \textit{Quality Assurance, QA}) es la definición de procesos y estándares que tienen como objetivo garantizar el desarrollo de software de calidad. También implica aplicar procesos de verificación y validación al producto obtenido. El aseguramiento de la calidad es utilizado en el proceso de gestión como herramienta para evaluar la adecuación del proceso de desarrollo a los planes de calidad establecidos. 

\imagen{gestion_calidad_proyecto.png}{Gestión de la calidad a nivel de proyecto.}

Normalmente el proceso de gestión de la calidad es independiente del proceso de desarrollo. La gestión de la calidad se encarga de comprobar los productos del proyecto para asegurar que son consistentes con los objetivos y los estándares establecidos. Es por ello, que el equipo de aseguramiento de calidad debe ser independiente del equipo de desarrollo, con el fin de evaluar el producto de forma objetiva.


\section{Estándares de calidad}
Los estándares juegan un papel muy importante en la gestión de la calidad. Una de las tareas del aseguramiento de la calidad de software es seleccionar los estándares que van a ser aplicados tanto al proceso de desarrollo como al producto final. La importancia de los estándares se debe a que aportan conocimiento acerca de prácticas apropiadas a tener en cuenta durante el proceso de desarrollo y proporcionan un marco para poder definir lo que es la calidad en cada proyecto particular. Existen dos tipos de estándares utilizados en la gestión de la calidad de software:

\begin{itemize}
	\item{\textbf{Estándares de producto:} son estándares que se aplican al producto. Incluyen estándares sobre la documentación, estándares de código y estándares sobre la estructura de documentos.}
	\item{\textbf{Estándares de proceso:} son estándares que definen los procesos a seguir durante el desarrollo de software. En ellos se especifican prácticas habituales, utilizadas en el desarrollo.}
\end{itemize}

\subsection{ISO 9126}\label{iso9126}
ISO 9126\footnote{ISO 9126 en Wikipedia: \url{https://en.wikipedia.org/wiki/ISO/IEC_9126}} es un estándar utilizado en la evaluación de la calidad de software. El estándar está dividido en cuatro partes que tratan los conceptos de \textit{modelo de calidad} \cite{iso_9126_1}, \textit{métricas externas} \cite{iso_9126_2}, \textit{métricas internas} \cite{iso_9126_3} y \textit{métricas de uso de calidad} \cite{iso_9126_4}, respectivamente.

En la primera parte del estándar se especifica un modelo de calidad de software de dos partes: \textit{Modelo de calidad para la Calidad Externa e Interna} y \textit{Modelo de calidad para Calidad en Uso}.
En estos modelos de calidad se identifican características clave para la calidad del software, que se clasifican en una estructura de atributos y subatributos. El estándar identifica seis atributos principales:
\begin{itemize}
	\item{\textbf{Funcionalidad:} es la capacidad del producto software de proporcionar funcionalidades que satisfacen las necesidades explícitas e implícitas al utilizarlo bajo determinadas condiciones. Este atributo a su vez se divide en los siguientes subatributos: \textit{Adecuación}, \textit{Exactitud}, \textit{Interoperabilidad}, \textit{Seguridad} y \textit{Cumplimiento funcional}.}
	\item{\textbf{Fiabilidad:} es la capacidad del producto software de mantener un nivel específico de prestaciones al utilizarlo bajo determinadas condiciones. La fiabilidad se divide en los siguientes subatributos: \textit{Madurez}, \textit{Tolerancia a fallos}, \textit{Recuperabilidad} y \textit{Cumplimiento de Fiabilidad}.}
	\item{\textbf{Usabilidad:} es la capacidad del producto software de ser aprendido, entendido y usado por el usuario. La usabilidad se divide en los siguientes subatributos: \textit{Aprendizaje}, \textit{Comprensión}, \textit{Operatividad} y \textit{Atractividad}.}
	\item{\textbf{Eficiencia:} es la capacidad del producto software de proporcionar la eficiencia apropiada, relativa a la cantidad de recursos utilizados, bajo condiciones específicas. La eficiencia a su vez se divide en los siguientes subatributos: \textit{Exactitud}, \textit{Comportamiento en el tiempo}, \textit{Utilización de recursos} y \textit{Cumplimiento de Eficiencia}.}
	\item{\textbf{Mantenibilidad:} es la capacidad de un producto software de ser mantenido o modificado. Las modificaciones pueden incluir correcciones, mejoras o adaptación de producto a un entorno concreto. La mantenibilidad se divide en los siguientes subapartados: \textit{Facilidad de análisis}, \textit{Facilidad de cambio}, \textit{Escalabilidad}, \textit{Facilidad de pruebas} y \textit{Cumplimiento de la Mantenibilidad}.}
	\item{\textbf{Portabilidad:} es la capacidad del producto software de ser transferido de un entorno a otro. La portabilidad se divide en los siguientes subapartados: \textit{Adaptabilidad}, \textit{Instalabilidad}, \textit{Coexistencia}, \textit{Capacidad de reemplazamiento} y \textit{Cumplimiento de la Portabilidad}.}
	\item{\textbf{Calidad en uso:} es la capacidad del producto software que permite a los usuarios conseguir sus objetivos de forma efectiva, productiva, segura y satisfactoria. La calidad de uso se divide en los siguientes subapartados: \textit{Efectividad}, \textit{Productividad}, \textit{Seguridad} y \textit{Satisfacción}.}
\end{itemize}

\imagen{modelo_calidad_externa_interna}{Modelo de calidad para la Calidad Externa e Interna}
\imagen{modelo_calidad_en_uso}{Modelo de calidad para la Calidad en Uso}


\section{QA en las Metodologías Ágiles}
Como se ha visto en los apartados anteriores, la gestión de la calidad se ocupa de establecer un marco de calidad a nivel de organización, a través de estándares, y de asegurar dicha calidad mediante la definición y aplicación de planes específicos a nivel de proyecto. Esta visión del aseguramiento de la calidad cobra más sentido en proyectos de gran tamaño, que utilizan metodologías clásicas de gestión en cascada, en las cuales hay un equipo independiente para cada fase del desarrollo del proyecto.

La forma de ver el aseguramiento de la calidad cambia en las metodologías ágiles de gestión por diversas razones: por un lado, en este tipo de metodologías no existen fases de desarrollo con equipos independientes, sino que hay un único equipo de trabajo reducido, sobre el que también recae la responsabilidad de asegurar la calidad. Por otro lado, los proyectos que utilizan este tipo de metodologías son más pequeños y su principal producto es código fuente. Por estas razones, es muy difícil aplicar estándares a gran escala.

La forma de asegurar la calidad en la gestión ágil es a través de la ejecución de pruebas sobre el código. En esta metodología de trabajo no existe un equipo encargado de realizar las pruebas sobre el producto, sino que son los propios desarrolladores los que se encargan de implementarlas y ejecutarlas. Además de la ejecución de pruebas, es habitual realizar revisiones de código en busca de defectos en el mismo. Para que las revisiones sean objetivas, se realizan por una persona ajena al desarrollo de la funcionalidad, p.ej otro desarrollador. Este proceso de revisiones puede automatizarse mediante herramientas de inspección, que calculan métricas  y localizan posibles defectos en el código en base a dichas métricas.


\section{Métricas de calidad}
Las métricas de calidad \cite{sommerville} son una serie de características del software, su documentación o el proceso de desarrollo que pueden medirse de forma objetiva. IEEE define formalmente este concepto como ''La medida cuantitativa del grado en que un sistema, componente o proceso posee un atributo dado''. Un ejemplo de métrica es el tamaño de un sistema en términos de líneas de código. A menudo el concepto de métrica es confundido con el concepto de \textit{medida}; la métrica es un atributo de un producto o proceso, mientras que la medida es el valor cuantitativo de dicho atributo.

Las métricas pueden clasificarse en dos grupos:
\begin{itemize}
	\item{\textbf{Métricas de control}:} este tipo de métricas son asociadas al proceso de desarrollo del software. Un ejemplo de este tipo de métricas es el esfuerzo medio, necesario para solucionar un defecto.

	\item{\textbf{Métricas de predicción}:} este tipo de métricas ayudan en la predicción de las características del software y son asociadas a él, por lo que a menudo se conocen como métricas de producto. Un ejemplo de este tipo de métrica son la complejidad ciclomática, número de atributos de un objeto o la profundidad de una clase en el árbol de herencia.
\end{itemize} 

\subsection{Deuda Técnica}
La deuda técnica \cite{deuda_tecnica_fowler} es un concepto desarrollado por Ward Cunningham como una metáfora de la deuda financiera para referirse al coste de trabajo incrementado en una aplicación al desarrollar de manera rapida y ''sucia'' con el fin de entregar una funcionalidad a tiempo. Este coste se refleja en la necesidad de mejorar el código ''sucio''. 

Actualmente este término se usa para referirse al coste de trabajo, en horas o en dinero, que es necesario invertir en un futuro en el mantenimiento de un sistema con un diseño pobre.

