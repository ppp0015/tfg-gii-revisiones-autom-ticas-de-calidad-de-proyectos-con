\apendice{Documentación de usuario}

\section{Introducción}
En este apéndice se explica de forma detallada cómo analizar proyectos utilizando las distintas herramientas de análisis (\textit{scanners}) de las que dispone SonarQube. En los apartados siguientes se dan a conocer los distintos \textit{scanners} utilizados por SonarQube, el proceso de instalación y configuración de los mismos y cómo realizar análisis de proyectos según el lenguaje de programación.

\section{Requisitos de usuarios}
Para poder realizar la instalación del entorno y el análisis de proyectos es necesario reunir una serie de requisitos por parte del usuario.

\subsection{Requisitos técnicos}
A continuación se muestra una lista de conocimientos técnicos que ha de reunir el usuario:
\begin{itemize}
	\item{Conocimientos básicos de comandos del sistema operativo Windows}
	\begin{itemize}
		\item{\textit{cd ''directorio''}: cambia el directorio de trabajo}
		\item{\textit{dir}: lista el contenido de un directorio}
		\item{\textit{rmdir /S ''directorio''}: elimina un directorio recursivamente}
		\item{\textit{del ''fichero''}: elimina un fichero}
		\item{\textit{setx ''variable'' ''valor''}: crea una variable de entorno o modifica su valor si existe}
		\item{etc.}
	\end{itemize}
	\item{Conocimientos básicos sobre la herramienta Maven}
\end{itemize}

\subsection{Requisitos software}\label{requisitos_software}
A continuación se muestra una lista con las herramientas necesarias y para qué se utiliza cada una de ellas:
\begin{itemize}

	\item{\textbf{SonarQube\cite{sq:web}:} es la base de la plataforma de evaluación. Para poder analizar proyectos, es necesario disponer de una instancia de la herramienta, bien en la máquina local o bien en un servidor, y que el plugin del lenguaje de programación utilizado en el desarrollo del proyecto, esté instalado en SonarQube.}

	\item{\textbf{SonarQube Scanner\cite{sq:scanner}:} es el \textit{scanner} por defecto, utilizado para analizar proyectos con SonarQube. Para analizar con este \textit{scanner}, es necesario tener Java instalado en la máquina local y que e plugin del lenguaje de programación del proyecto esté instalado en el servidor de SonarQube.}

	\item{\textbf{SonarQube Scanner for MSBuild\cite{sq:scanner-msbuild}:} este \textit{scanner} se utiliza para el análisis de proyectos .NET realizados con Visual Studio de Microsoft, utilizando el lenguaje de programación  C\# o Visual Basic .NET. Para poder analizar con este \textit{scanner} es necesario tener instalado el entorno de desarrollo Visual Studio 2015, que incorpora una serie de herramientas utilizadas durante el análisis. Otro requisito importante para poder analizar con este \textit{scanner} es, que el plugin del lenguaje de programación del proyecto esté instalado en el servidor de SonarQube.}

	\item{\textbf{Apache Maven\cite{maven:web}:} esta herramienta se utiliza en la gestión de proyectos realizados en Java. También permite lanzar análisis de proyectos con SonarQube a través de objetivos (\textit{goals}). Para poder utilizar Maven, es necesario tener Java instalado en la máquina y que el proyecto a analizar utilice esta herramienta en el despliegue y construcción.}

	\item{\textbf{Java\cite{java:web}:} Java es un lenguaje de programación ampliamente utilizado en el desarrollo de aplicaciones. Proporciona una plataforma para la ejecución de aplicaciones llamada \textit{Java Runtime Environment (JRE)} y un API para el desarrollo llamado \textit{Java Development Kit (JDK)}. En el proyecto, aparte para el desarrollo de un plugin, la plataforma de Java es utilizada para ejecutar el servidor de SonarQube y las herramientas Maven y SonarQube Scanner.}

	\item{\textbf{Visual Studio 2015\cite{vs2015:web}:} es un entorno de programación proporcionado por Microsoft para el desarrollo de aplicaciones en los lenguajes de programación C\# o Visual Basic .NET, que además incluye una serie de librerías y herramientas como MSBuild 14.0, utilizadas en el análisis de proyectos por la herramienta SonarQube Scanner for MSBuild.} 
\end{itemize}

\section{Instalación}\label{instalacion}
 A continuación se describe el proceso de instalación y configuración de cada una de las herramientas del apartado \textit{\ref{requisitos_software} - Requisitos software} para el sistema operativo Windows 7 de 64 bits, ya que es el que se ha utilizado en el desarrollo del proyecto.

\subsection{Java}
La distribución de Java\cite{java:web} utilizada en este trabajo es la proporcionada por Oracle, puesto que tiene un uso muy extendido. La última versión puede descargarse desde la página oficial de Oracle\footnote{Página de descarga de Java: \url{http://www.oracle.com/technetwork/java/javase/downloads/index.html}} e incorpora tanto la máquina virtual de Java (\textit{JRE}), como el kit de desarrollo (\textit{JDK}). Hay que asegurarse de descargar la versión correcta para el sistema operativo sobre el que se instalará Java (en este caso Windows 7 de 64 bits).  Una vez descargada la versión correcta de Java para el sistema operativo, se ejecuta el instalador y se siguen los pasos de instalación. En las ventanas que lo soliciten, elegir el directorio de instalación deseado.

Después de terminar la instalación, se comprueba que Java haya sido instalado correctamente con el comando \textit{java -version}(ver figura \ref{fig:version_java.png}).
\imagen{version_java.png}{Versión instalada de Java}

Por último se crea la variable de entorno \textit{\%JAVA\_HOME\%} y se añade al \textit{path} del sistema con los siguientes comandos: 
\begin{itemize}
	\item{setx JAVA\_HOME ''directorio\_instalacion\_jdk''}
	\item{setx PATH ''\%PATH\%;\%JAVA\_HOME\%\textbackslash bin''}
\end{itemize}


\subsection{Visual Studio 2015}
Para instalar Visual Studio\cite{vs2015:web} se utiliza la versión del año 2015\footnote{Página de descarga de Visual Studio 2015: \url{https://www.visualstudio.com/vs-2015-product-editions}}. Una vez descargado el instalador se ejecuta y se siguen los pasos de instalación del mismo. En la ventana de selección del directorio de instalación (ver figura \ref{fig:instalacion_vs2015.png}) se elige el directorio deseado y se deja marcada la opción por defecto.

\imagen{instalacion_vs2015.png}{Instalación de Visual Studio 2015: directorio de instalación}

Después de instalar Visual Studio 2015 es necesario reiniciar la máquina.


\subsection{SonarQube Scanner}
Para la instalación de SonarQube Scanner\cite{sq:scanner} se utiliza la última versión disponible en la página oficial del mismo. Después de descargar el fichero \textbf{sonar-scanner-2.6.zip}, se descomprime en el directorio de instalación deseado y se configura el \textit{path} del sistema, creando la variable de entorno  \textit{\%SONAR\_SCANNER\_HOME\%}:
\begin{itemize}
	\item{setx SONAR\_SCANNER\_HOME ''directorio\_instalacion\_sonar\_scanner''}
	\item{setx PATH ''\%PATH\%;\%SONAR\_SCANNER\_HOME\%\textbackslash bin''}
\end{itemize}

Después de configurar las variables de entorno se comprueba que el \textit{scanner} haya sido instalado correctamente, ejecutando el comando \textit{sonar-scanner -v} (ver figura \ref{fig:version_sonar_scanner.png}).

\imagen{version_sonar_scanner.png}{Versión instalada de SonarQube Scanner}


\subsection{SonarQube Scanner for MSBuild}
Antes de instalar SonarQube Scanner for MSBuild\cite{sq:scanner-msbuild} es necesario haber instalado las herramientas MSBuild 14.0 de Microsoft para poder trabajar con el \textit{scanner}. Puesto que vienen integradas en la instalación de Visual Studio 2015, no es necesario realizar ninguna otra instalación. Para instalar SonarQube Scanner for MSBuild\cite{sq:scanner-msbuild} se utiliza la última versión disponible en la página oficial del mismo. Después de descargar el fichero \textbf{MSBuild.SonarQube.Runner-2.0.zip}, se descomprime en el directorio de instalación deseado y se configura el \textit{path} del sistema, creando la variable de entorno  \textit{\%SONAR\_SCANNER\_MSBUILD\%}:
\begin{itemize}
	\item{setx SONAR\_SCANNER\_MSBUILD ''directorio\_instalacion''}
	\item{setx PATH ''\%PATH\%;\%SONAR\_SCANNER\_MSBUILD\%\textbackslash bin''}
\end{itemize}

Después de configurar las variables de entorno se comprueba que el \textit{scanner} haya sido instalado correctamente, ejecutando el comando \textit{msbuild.sonarqube.runner /v} (ver figura \ref{fig:version_msbuild_scanner.png}).

\imagen{version_msbuild_scanner.png}{Versión instalada de SonarQube Scanner for MSBuild}


\subsection{SonarQube Scanner for Maven}
Para poder utilizar el analizador SonarQube Scanner for Maven\cite{sq:scanner-maven} no es necesario realizar ningufna instalación. Basta con incluir las dependencias al plugin \textit{sonar-maven-plugin} en el fichero de configuración \textit{pom.xml} del proyecto a analizar. 

\subsection{Apache Maven}
Para la instalación de Maven\cite{maven:web} se utiliza la última versión de la herramienta, disponible en la página oficial\footnote{Página de descarga de Maven:\url{http://maven.apache.org/download.cgi}}. Después de descargar el fichero \textbf{apache-maven-3.3.9-bin.zip}, se descomprime en el directorio de instalación. Por último, se crea la variable de entorno \textit{\%MVN\_HOME\%} y se añade al \textit{path} del sistema con los siguientes comandos:
\begin{itemize}
	\item{setx MVN\_HOME ''directorio\_instalacion\_maven''}
	\item{setx PATH ''\%PATH\%;\%MVN\_HOME\%\textbackslash bin''}
\end{itemize}

Después de configurar las variables de entorno, se comprueba que Maven haya sido instalado correctamente, ejecutando el comando \textit{mvn --version} (ver figura \ref{fig:version_maven.png}).

\imagen{version_maven.png}{Versión instalada de Maven}


\section{Manual del usuario: evaluación de calidad de proyectos}
En este apartado se explica cómo analizar proyectos, utilizando los distintos \textit{scanners} que ofrece SonarQube.
Dependiendo del proyecto que se quiere analizar, se utiliza un \textit{scanner} u otro. Para el análisis es imprescindible disponer de una instancia de SonarQube(ver apéndice \textit{\ref{manual_administrador} - Documentación de administrador}) y que el plugin para el lenguaje de programación utilizado en el proyecto esté instalado en la instancia.
 A continuación se describen los pasos para los distintos analizadores utilizados por SonarQube.

\subsection{SonarQube Scanner}
Una vez instalado SonarQube Scanner\cite{sq:scanner}, el análisis de un proyecto se realiza siguiendo los siguientes pasos:

\begin{itemize}
	\item{Se copia el fichero \textbf{sonar-project.properties}\footnote{Directorio ''sonarqube/scanner/sonar scanner config'' del proyecto} en el directorio raíz del proyecto y se configura de forma que se ajuste a las características del mismo (nombre del proyecto, identificador único, ruta a los ficheros fuente, etc.).}
	\item{Se compila el proyecto en caso de haber especificado ficheros binarios en el fichero de configuración.}
	\item{Desde línea de comandos se ejecuta el comando \textit{sonar-scanner} en el directorio raíz del proyecto.}
\end{itemize}

Cuando haya concluido el análisis, los resultados pueden verse en la instancia de SonarQube, a través de la \textit{url} especificada en el fichero \textbf{sonar-project.properties}.

Si se desea analizar cobertura de código, es necesario obtener los informes de cobertura mediante herramientas utilizadas para cada lenguaje de programación e indicar en el fichero de configuración \textbf{sonar-project.properties} la ruta a dichos ficheros. En el caso de proyectos Java, este proceso puede realizarse automáticament con la herramienta Maven.

\subsection{SonarQube Scanner for MSBuild}
Una vez instalado SonarQube Scanner for MSBuild\cite{sq:scanner-msbuild}, el análisis de un proyecto se realiza siguiendo los siguientes pasos:

\begin{itemize}
	\item{Se copia el fichero \textbf{SonarQube.Analysis.xml}\footnote{Directorio ''sonarqube/scanner/msbuild scanner config'' del proyecto} en el directorio raíz del proyecto y se configura de forma que se ajuste a las necesidades del mismo.}
	\item{Se abre el terminal de desarrollo de Visual Studio (\textit{Developer Command Prompt}) seleccionando \textbf{Inicio\textgreater Programas\textgreater Visual Studio 2015\textgreater Developer Command Prompt} y se accede al directorio raíz del proyecto.}
	\item Se ejecuta el \textit{scanner} con el comando \textit{MsBuild.SonarQube.Runner.exe begin} y se especifican  los siguientes parámetros: 
		\begin{itemize}
			\item{/k:''clave\_proyecto'' (identificador único del proyecto en SonarQube)}
			\item{/n:''nombre\_proyecto'' (nombre del proyecto)}
			\item{/v:''version\_proyecto'' (versión del proyecto)}
			\item{/s:''\%CD\%\textbackslash SoarQube.Analysis.xml''}		
		\end{itemize}
	\item{Se compila el proyecto con el comando \textit{msbuild}.}
	\item{Se generan informes de test unitarios y cobertura con las herramientas correspondietes, en caso de querer incluirlos en el análisis.}
	\item{Se detiene el \textit{scanner} con el comando \textit{MsBuild.SonarQube.Runner.exe end}.}
\end{itemize}

Para incluir los resultados de pruebas unitarias y cobertura de código en el análisis con SonarQube hay que configurar los parámetros correspondientes a las herramientas utilizadas para generarlos, especificando la ruta al fichero con los resultados.

Una vez concluido el análisis, los resultados pueden verse en la instancia de SonarQube, a través de la \textit{url} especificada durante el análisis.


\subsection{SonarQube Scanner for Maven}
SonarQube Scanner for Maven\cite{sq:scanner-maven} es un plugin utilizado desde Maven para el análisis de proyectos. Precisamente uno de los requisitos para este \textit{scanner} es utilizar Maven como herramienta de gestión del proyecto. Para poder analizar proyectos con SonarQube Scanner for Maven, se deben configurar las siguientes propiedades específicas de SonarQube en la sección \textit{properties} del fichero \textbf{pom.xml}:
\begin{itemize}
	\item{\textbf{sonar.host.url}: esta propiedad se utiliza para indicar la \textit{url} a la instancia de SonarQube contra la que se realiza el análisis. Si no se indica esta propiedad se toma el valor por defecto (http://localhost:9000) y es necesario tener desplegada la instancia en la máquina local.}
	\item{\textbf{sonar.language}: esta propiedad se utiliza para indicar al \textit{scanner} el lenguaje de programación del proyecto y por lo tanto es una propiedad obligatoria.}
\end{itemize}

Como se puede observar, no es necesario configurar las dependencias del plugin en el fichero \textbf{pom.xml} ya que, el servidor de SonarQube incluye un repositorio Maven\footnote{Artículo \textit{We had a dream: mvn sonar:sonar}: \url{http://www.sonarqube.org/we-had-a-dream-mvn-sonarsonar/}}. De esta forma se utiliza la última versión disponible del plugin. Si se quiere utilizar una versión en concreto, hay que incluir la dependencia en el fichero \textbf{pom.xml}

Para realizar un análisis desde Maven, se deben realizar los siguientes pasos:
\begin{itemize}
	\item{Se realiza una instalación limpia del proyecto con el comando \textit{mvn clean install}}
	\item{Se ejecuta el análisis con el comando \textit{mvn sonar:sonar}}
\end{itemize}

También es posible obtener cobertura de código mediante la ejecución de pruebas durante la instalación del proyecto. Esto se hace a través de la configuración de una serie de plugins en el fichero \textbf{pom.xml}. Debido a su extensión, se proporcona un fichero genérico\footnote{Directorio ''sonarqube/scanner/maven config'' del proyecto} con las dependencias y configuraciones de dichos plugins, que se pueden añadir al fichero \textbf{pom.xml} del proyecto.

Una vez concluido el análisis, los resultados pueden verse en la instancia de SonarQube, a través de la \textit{url} especificada en el fichero \textbf{pom.xml}.

\subsection{Comparación de proyectos}
SonarQube dispone de funcionalidad que permite comparar los proyectos medidos en la plataforma entre sí, permitiendo seleccionar las métricas por las cuales se van a comparar los proyectos. Para ello, proporciona un \textit{dashboard} específico en el cual se seleccionan los proyectos a comparar.

El procedimiento para comparar proyectos en la plataforma es el siguiente:
\begin{itemize}
	\item{En la instancia de SonarQube se elige \textbf{More\textgreater Compare Projects.}}
	\item{En el \textit{dashboard} que se abre se selecciona \textbf{Add project} y se seleccionan los proyectos a comparar.}
	\item{Se seleccionan las métricas por las cuales se quieren comparar los proyectos a través del campo \textbf{Add metric}.}
\end{itemize}
