En este directorio se encuentran los scripts de configuración, arranque y parada
de la instancia de SonarQube en la plataforma OpenShift.

Uso: copiar el directorio .openshift en el repositorio git obtenido tras crear
la aplicación para la instancia de SonarQube con las herramientas RHC de OpenShift.