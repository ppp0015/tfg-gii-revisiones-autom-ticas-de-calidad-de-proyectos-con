#!/bin/bash

# This script is an Action Hook for OpenShift platform used to 
# install and configure SonarQube files on OpenShift. The script
# configures SonarQube modifying sonar.properties and wrapper.conf
# files in order to meet OpenShift platform requirements.
# It is automatically executed by OpenShift.
# Author: Plamen Petyov Petkov


# Cd to the SonarQube conf directory, where
# sonar.properties and wrapper.conf are located
cd $OPENSHIFT_REPO_DIR/sonarqube-5.4/conf

# Remove the existing properties fila and creates
# new one for the configurations
rm sonar.properties; touch sonar.properties


##### Configuring database connection
echo "Configuring database connection ..."

# PostgreSQL database URL
DB_URL="$OPENSHIFT_POSTGRESQL_DB_HOST:$OPENSHIFT_POSTGRESQL_DB_PORT/$PGDATABASE"

echo -e "# Database configurations\n" >> sonar.properties
# Configures the username, using OPENSHIFT_POSTGRESQL_DB_USERNAME environment variable
echo -e "sonar.jdbc.username=$OPENSHIFT_POSTGRESQL_DB_USERNAME\n" >> sonar.properties
# Configures the password, using OPENSHIFT_POSTGRESQL_DB_PASSWORD environment variable
echo -e "sonar.jdbc.password=$OPENSHIFT_POSTGRESQL_DB_PASSWORD\n" >> sonar.properties
# Configures the database url, using DB_URL variable from the script
echo -e "sonar.jdbc.url=jdbc:postgresql://$DB_URL\n" >> sonar.properties


##### Configuring SonarQube server
echo "Configuring SonarQube server ..."

echo -e "\n# SonarQube Server configurations\n" >> sonar.properties
# Configures server IP address, using OPENSHIFT_DIY_IP environment variable
echo -e "sonar.web.host=$OPENSHIFT_DIY_IP\n" >> sonar.properties
# Configures server IP address, using OPENSHIFT_DIY_PORT environment variable
echo -e "sonar.web.port=$OPENSHIFT_DIY_PORT\n" >> sonar.properties

##### Configuring Elastic Search server (used for searches in SonarQube instance)
echo "Configuring Elastic Search server ..."

echo -e "\n# Search Server Configuration\n" >> sonar.properties
# Configures search server IP address, using OPENSHIFT_DIY_IP environment variable
echo -e "sonar.search.host=$OPENSHIFT_DIY_IP\n" >> sonar.properties
# Configures search server port
echo -e "sonar.search.port=15000\n" >> sonar.properties

##### Configuring JVM options
echo "Configuring JVM options ..."

echo -e "\n# JVM options configuration\n" >> sonar.properties
# Configures min and max memory on JVM for SonarQube instance
echo -e "sonar.web.javaOpts=-Xmx384m -Xms384m\n" >> sonar.properties
# Configures min and max memory on JVM for Elastic Search
echo -e "sonar.search.javaOpts=-Xmx128m -Xms128m\n" >> sonar.properties

##### Configuring wrapper.conf
echo "Configuring wrapper ..."
# Configures JWS wrapper to use pipe instead of sockets
echo -e "\nwrapper.backend.type=PIPE" >> wrapper.conf

##### Configuring permissions of executable files
cd $OPENSHIFT_REPO_DIR/sonarqube-5.4/bin/linux-x86-64
chmod 755 sonar.sh
chmod 755 wrapper-linux-x86-64
