En este directorio se encuentran dos plugins desarrollados para la plataforma
de SonarQube:
	- sonar-helloworld-plugin: es un plugin de ejemplo que incluye los componentes
	básicos de los que consta un plugin de SonarQube.
	- sonar-ubuwidgets-plugin: es un plugin que incluye únicamente una vista
	(widget) con el logo de la Universidad de Burgos y permite incluir un texto
	de presentación. Este plugin puede ser desarrollado para incluir más componentes.

