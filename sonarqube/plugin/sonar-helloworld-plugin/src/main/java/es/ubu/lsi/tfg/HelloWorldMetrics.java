package es.ubu.lsi.tfg;

import org.sonar.api.internal.google.common.collect.ImmutableList;
import org.sonar.api.measures.CoreMetrics;
import org.sonar.api.measures.Metric;
import org.sonar.api.measures.Metrics;

import java.util.Arrays;
import java.util.List;

public class HelloWorldMetrics implements Metrics {

    // TODO: define public static final Metric instances for each desired metric.

    /**
     * Default constructor.
     */
    public HelloWorldMetrics() {
        super();
    }

    @Override
    public List<Metric> getMetrics() {
        // Uses SonarQube Metrics
        // Returned List could be of any concrete implementation
        return Arrays.<Metric>asList(CoreMetrics.LINES, CoreMetrics.CLASSES, CoreMetrics.PUBLIC_API);
    }
}
