package es.ubu.lsi.tfg;

import org.sonar.api.SonarPlugin;

import java.util.Arrays;
import java.util.List;

/**
 * Hello world!
 *
 */
public class HelloWorldPlugin extends SonarPlugin {

    @Override
    public List getExtensions() {
        // Add extensions for the SonarQube Plugin
        // Returned List could be of any concrete implementation
//        return Arrays.asList(HelloWorldMetrics.class, HelloWorldWidget.class);
        return Arrays.asList(HelloWorldWidget.class);
    }
}
