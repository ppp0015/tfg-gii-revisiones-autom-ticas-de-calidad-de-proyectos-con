package es.ubu.lsi.tfg;

import org.sonar.api.batch.Sensor;
import org.sonar.api.batch.SensorContext;
import org.sonar.api.batch.fs.FileSystem;
import org.sonar.api.config.Settings;
import org.sonar.api.resources.Project;

// HERE GOES THE LOGIC OF FILE PROCESSING FOR GETTING METRICS
public class HelloWorldSensor implements Sensor {

    private FileSystem fs;
    private Settings settings;

    public HelloWorldSensor(FileSystem fs, Settings settings) {
        // TODO: add sensor constructs
        this.fs = fs;
        this.settings = settings;
    }

    @Override
    public void analyse(Project project, SensorContext sensorContext) {
        // Analysis logic goes here for all the project
        // SAVING metrics to database goes HERE.
    }

    @Override
    public boolean shouldExecuteOnProject(Project project) {
        // CHECK WHETHER OR NOT TO EXECUTE THE SENSOR DURING PROJECT ANALYSIS
        return true;
    }
}
