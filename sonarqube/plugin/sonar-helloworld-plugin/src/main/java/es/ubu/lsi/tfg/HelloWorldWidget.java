package es.ubu.lsi.tfg;


import org.sonar.api.web.*;

@UserRole(UserRole.USER)
@Description("An example of HelloWorld plugin for SonarQube platform.")
@WidgetCategory("Examples")
public class HelloWorldWidget extends AbstractRubyTemplate implements RubyRailsWidget {

    /**
     * Default constructor.
     */
    public HelloWorldWidget() {
        super();
    }

    /**
     * Path to the Ruby widget template file.
     *
     * @return path to the Ruby widget template file.
     */
    @Override
    protected String getTemplatePath() {
        String templatePath = "/es/ubu/lsi/tfg/helloworld/hello_world_widget.html.erb";
        return templatePath;
    }

    /**
     * Widget ID in SonarQube dashboard.
     *
     * @return widget's ID
     */
    @Override
    public String getId() {
        return "helloworld";
    }

    /**
     * Widget title in SonarQube dashboard.
     *
     * @return widget's title
     */
    @Override
    public String getTitle() {
        return "HelloWorld Widget";
    }
}
