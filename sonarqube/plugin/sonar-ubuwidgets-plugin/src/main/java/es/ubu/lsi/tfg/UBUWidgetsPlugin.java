package es.ubu.lsi.tfg;

import es.ubu.lsi.tfg.widgets.UBUWelcomeWidget;
import org.sonar.api.SonarPlugin;

import java.util.Arrays;
import java.util.List;

/**
 * UBUWidgets plugin main class.
 *
 * This class is the plugin entry point and declares a list of extensions
 * used by the plugin.
 *
 * @author Plamen Petyov Petkov
 */
public class UBUWidgetsPlugin extends SonarPlugin {

    /**
     * Default constructor.
     */
    public UBUWidgetsPlugin() {}

    /**
     * Returns a list of extensions for the plugin.
     *
     * @return list of extensions
     */
    @Override
    public List getExtensions() {
        return Arrays.asList(UBUWelcomeWidget.class);
    }
}