package es.ubu.lsi.tfg.widgets;

import org.sonar.api.web.*;

/**
 * Class UBUWelcomeWidget.
 *
 * @author Plamen Petyov Petkov
 */
@UserRole(UserRole.USER)
@WidgetScope({WidgetScope.GLOBAL})
@WidgetCategory(UBUWelcomeWidget.WIDGET_CATEGORY)
@Description(UBUWelcomeWidget.WIDGET_DESCRIPTION)
@WidgetProperties({
        @WidgetProperty(
                key = "Message",
                type = WidgetPropertyType.TEXT,
                optional = false,
                description = "Message to be displayed using markdown text" +
                        "(<a href='#' onclick=\"window.open(baseUrl + '/markdown/help','markdown','height=300,width=600,scrollbars=1,resizable=1');return false;\">see help</a>)"
        )
})
public class UBUWelcomeWidget extends AbstractRubyTemplate implements RubyRailsWidget {

    public static final String WIDGET_CATEGORY = "UBU Widgets";
    public static final String WIDGET_DESCRIPTION = "Displays the logo of UBU and " +
            "a welcome message written by the user.";

    /**
     * Default constructor.
     */
    public UBUWelcomeWidget() {}

    /**
     * Returns widget's ID.
     *
     * @return widget's ID
     */
    @Override
    public String getId() {
        return "ubu_welcome_widget";
    }

    /**
     * Returns widget's title.
     *
     * @return widget's title
     */
    @Override
    public String getTitle() {
        return "UBU Welcome Widget";
    }

    /**
     * Returns the relative path to widget's template file.
     *
     * @return path to template file
     */
    @Override
    protected String getTemplatePath() {

        // Use absolute path for debug mode
        return "/es/ubu/lsi/tfg/widgets/ubu_welcome_widget.html.erb";
    }
}