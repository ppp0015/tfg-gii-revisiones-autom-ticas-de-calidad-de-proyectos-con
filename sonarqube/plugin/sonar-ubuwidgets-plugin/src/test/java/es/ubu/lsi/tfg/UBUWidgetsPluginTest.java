package es.ubu.lsi.tfg;

import es.ubu.lsi.tfg.widgets.UBUWelcomeWidget;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Test case for class UBUWidgetPlugin.
 *
 * @author Plamen Petyov Petkov
 */
public class UBUWidgetsPluginTest {

    private UBUWidgetsPlugin plugin;

    @Before
    public void setUp() throws Exception {
        plugin = new UBUWidgetsPlugin();
        assertNotNull(plugin);
    }

    @Test
    public void testGetExtensions() throws Exception {
        assertNotNull(plugin.getExtensions());
        assertEquals(plugin.getExtensions().size(), 1);
        assertTrue(plugin.getExtensions().contains(UBUWelcomeWidget.class));
    }
}