package es.ubu.lsi.tfg.widgets;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Test case for class UBUWelcomeWidget.
 *
 * @author Plamen Petyov Petkov
 */
public class UBUWelcomeWidgetTest {

    private UBUWelcomeWidget widget;

    @Before
    public void setUp() throws Exception {
        widget = new UBUWelcomeWidget();
        assertNotNull(widget);
    }

    @Test
    public void testGetId() throws Exception {
        assertNotNull(widget.getId());
        assertNotEquals(widget.getId(), "");
        assertEquals(widget.getId(), "ubu_welcome_widget");
    }

    @Test
    public void testGetTitle() throws Exception {
        assertNotNull(widget.getTitle());
        assertNotEquals(widget.getTitle(), "");
        assertEquals(widget.getTitle(), "UBU Welcome Widget");
    }

    @Test
    public void testGetTemplatePath() throws Exception {
        String templatePath = "/es/ubu/lsi/tfg/widgets/ubu_welcome_widget.html.erb";
        assertNotNull(widget.getTemplatePath());
        assertNotEquals(widget.getTemplatePath(), "");
        assertEquals(widget.getTemplatePath(), templatePath);
    }
}