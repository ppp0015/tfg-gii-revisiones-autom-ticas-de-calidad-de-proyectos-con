En este directorio se encuentran los ficheros de configuración de proyectos
para el análisis con SonarQube. Cada uno de los ficheros se usa por un 
scanner diferente. Para el análisis se copia el fichero de configuración
al directorio raíz del proyecto y se configura según las necesidades.

Ficheros de configuración:
	- pom.xml: 						Usado por SonarQube Scanner for Maven (leer descripción en el fichero)
	- sonar-project.properties: 	Usado por SonarQube Scanner
	- SonarQube.Analysis.xml:		Usado por sonarQube Scanner for MSBuild