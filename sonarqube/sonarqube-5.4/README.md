En este directorio se encuentran los ficheros de SonarQube, versión 5.4.
Éstos ficheros han sido configurados (consultar anexo Manual del Administrador)
para que la herramienta pueda ser utilizada en la plataforma OpenShift.

Uso: copiar el directorio sonarqube-5.4 en el repositorio git obtenido tras crear
la aplicación para la instancia de SonarQube con las herramientas RHC de OpenShift.